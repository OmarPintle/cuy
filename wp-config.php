<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7>/n+|Xd5{a+wG,`X~D>>C+:xS*]|boQm@4J5XM@4xS{c5=k$@+I6bh6/STwFXG4' );
define( 'SECURE_AUTH_KEY',  '=dr!qV`u>_pA3(l9HHNAS+f lp]dE]^b~gNb{.^6TI-YtL9F`FE6n~+a8tcPev~]' );
define( 'LOGGED_IN_KEY',    '3*5uE$Fx=w>VM!CNV|-SlsW*D6iV,siY}P+#Ko<aRd2TXw2@2+PO}rT!&28pQf/?' );
define( 'NONCE_KEY',        'q_AbZQ;fj5>ufZLxOrvp104-z7.DE0<K.;9zF:#- Y:Ovwh)LBF>eRs0hppGkW/Q' );
define( 'AUTH_SALT',        'O;@Kt*4#tVkwf.nY|SnBx7z3BVv_[-D4Y)dE;TISo{GMM+p^*)^J*_=N.A.5Td{R' );
define( 'SECURE_AUTH_SALT', 'y|qITShWKF2{[;hP.g [8DI>&-N0|4}vZ, w^@{wP^%+f0!sR&n<7%ALt*gaU3M-' );
define( 'LOGGED_IN_SALT',   'LN_!:4btPIU9>;Rv5o5{%asa6:9|J>UU=KMn}mj3Y[BZ6as%[Rfm=Y+tM@i-)ll<' );
define( 'NONCE_SALT',       'tV&$C_U]0v+, I|*8+ng$~k;hQrhc@@Eeb.JgKp]ax=CaxKE^aXxdWSSI`_GlR:(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
