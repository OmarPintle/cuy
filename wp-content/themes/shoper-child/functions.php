<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'bootstrap','icofont','scrollbar','shoper-common' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

function Titulo_Principal(){
    echo "EL rey del comic";
}
add_action('shoper_header_layout_1_branding', 'Titulo_Principal');

function Tienda(){
    echo "COMICS";
}
add_action('woocommerce_archive_description', 'Tienda');

function Feliz_Dias(){
$day = date("D");
if ($day == "fri"){
    echo "Tenga un feliz dia!";
    }
    elseif($day =="Sun"){
        echo "Feliz lunes!";

    }
    else{
        echo "Feliz dia Comic leyentes";
    }
}
add_action('woocommerce_before_shop_loop', 'Feliz_Dias');

